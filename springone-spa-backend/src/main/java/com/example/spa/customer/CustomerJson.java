package com.example.spa.customer;

import org.hibernate.validator.constraints.Length;

public class CustomerJson {

	private Integer id;

	@Length(min=0, message="name must be 10 characters long")
	private String name;
	private String email;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
