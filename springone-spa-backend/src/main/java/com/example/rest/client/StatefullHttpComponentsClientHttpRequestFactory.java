package com.example.rest.client;

import java.net.URI;

import org.apache.http.client.HttpClient;
import org.apache.http.protocol.HttpContext;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

/**
 * This class is passed to spring to create an http client that understands cookies.
 * 
 * @author Adib Saikali
 * 
 */
public class StatefullHttpComponentsClientHttpRequestFactory extends HttpComponentsClientHttpRequestFactory
{
	private final HttpContext httpContext;

	public StatefullHttpComponentsClientHttpRequestFactory(HttpClient httpClient, HttpContext httpContext)
	{
		super(httpClient);
		this.httpContext = httpContext;
	}

	@Override
	protected HttpContext createHttpContext(HttpMethod httpMethod, URI uri)
	{
		return this.httpContext;
	}
}
