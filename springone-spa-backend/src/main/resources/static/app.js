$(function() {

    //session timeout test
    $('.expireBtn').click(function () {
        $('.section1 pre').append('<div>GET: /expire</div>');
        $.get('/expire').always(function (xhr) {
            var resp = (xhr && xhr.responseJSON)?xhr.responseJSON:xhr;
            $('.section1 pre').append('<div>RESPONSE: '+JSON.stringify(resp)+'</div><hr/>');
        });
    });

    $('.doStuff0').click(function () {
        $('.section1 pre').append('<div>GET: /api/customers/1</div>');
        $.get('/api/customers/1').always(function (xhr) {
            var status = xhr.status +' ' + xhr.statusText;
            var resp = (xhr && xhr.responseJSON)?xhr.responseJSON:xhr;
            $('.section1 pre').append('<div>RESPONSE: '+status+' <br/>'+JSON.stringify(resp)+'</div><hr/>');
        });
    });



    //API calls test
    $('.doStuff1').click(function () {
        var url = '/api/customers/1';
        $('.section2 pre').append('<div>GET: '+url+'</div>');

        $.get(url).always(function (xhr) {
            var status = xhr.status +' ' + xhr.statusText;
            var resp = (xhr && xhr.responseJSON)?xhr.responseJSON:xhr;
            $('.section2 pre').append('<div>RESPONSE: '+status+' <br/>'+JSON.stringify(resp)+'</div><hr/>');
        });
    });

    $('.doStuff2').click(function () {
        var url = '/api/customers';
        var data = JSON.stringify(
            {
                name:"Jane",
                email:"jane@mail.com"
            }
        );
        $('.section2 pre').append('<div>POST: '+url+', '+data+'</div>');
        $.ajax(url, {
            data :data,
            contentType : 'application/json',
            type : 'POST'
        }).always(function (xhr) {
            var status = xhr.status +' ' + xhr.statusText;
            var resp = (xhr && xhr.responseJSON)?xhr.responseJSON:xhr;
            $('.section2 pre').append('<div>RESPONSE: '+status+' <br/>'+JSON.stringify(resp)+'</div><hr/>');
        });
    });


    $('.doStuff3').click(function () {
        var url = '/api/customers/1';
        var data = JSON.stringify(
            {
                id:1,
                name:"Jane",
                email:"jane@mail.com"
            }
        );
        $('.section2 pre').append('<div>PUT: '+url+', '+data+'</div>');
        $.ajax(url, {
            data :data,
            contentType : 'application/json',
            type : 'PUT'
        }).always(function (xhr) {
            var status = xhr.status +' ' + xhr.statusText;
            var resp = (xhr && xhr.responseJSON)?xhr.responseJSON:xhr;
            $('.section2 pre').append('<div>RESPONSE: '+status+' <br/>'+JSON.stringify(resp)+'</div><hr/>');
        });
    });


    $('.doStuff4').click(function () {
        var url = '/api/customers/1';
        $('.section2 pre').append('<div>DELETE: '+url+'</div>');
        $.ajax(url, {
            contentType : 'application/json',
            type : 'DELETE'
        }).always(function (xhr) {
            var status = xhr.status +' ' + xhr.statusText;
            var resp = (xhr && xhr.responseJSON)?xhr.responseJSON:xhr;
            $('.section2 pre').append('<div>RESPONSE: '+status+' <br/>'+JSON.stringify(resp)+'</div><hr/>');
        });
    });


    /* CLEAN CONSOLES */
    $('.doStuff00').click(function () {
        $('.section1 pre').html('');
    });
    $('.doStuff5').click(function () {
        $('.section2 pre').html('');
    });
    $('.doStuff8').click(function () {
        $('.section3 pre').html('');
    });




    /* ECHO SERVICES*/
    $('.doStuff6').click(function () {
        var url = '/echo/add';
        var data = JSON.stringify(
            {
                    url:"/echo/api/test",
                    method:"GET",
                    responseCode:"200",
                    //responseBody: {id:getRandomInt(1,100), name:"Bob-"+getRandomInt(1,100)},
                    responseBody: '{id:1,name:"Bob"}',
                    responseContentType:'application/json'
            }
        );
        $('.section3 pre').append('<div>POST: '+url+', '+data+'</div>');
        $.ajax(url, {
            data :data,
            contentType : 'application/json',
            type : 'POST'
        }).always(function (xhr) {
            var status = xhr.status +' ' + xhr.statusText;
            var resp = (xhr && xhr.responseJSON)?xhr.responseJSON:xhr;
            $('.section3 pre').append('<div>RESPONSE: '+status+' <br/>'+JSON.stringify(resp)+'</div><hr/>');
        });
    });


    $('.doStuff7').click(function () {
        var url = '/echo/api/test';
        $('.section3 pre').append('<div>GET: '+url+'</div>');
        $.get(url).always(function (xhr) {
            var status = xhr.status +' ' + xhr.statusText;
            var resp = (xhr && xhr.responseJSON)?xhr.responseJSON:xhr;
            $('.section3 pre').append('<div>RESPONSE: <br/>'+JSON.stringify(resp)+'</div><hr/>');
        });
    });


    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
});